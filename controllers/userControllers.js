const User = require("../models/User")
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration Controller
module.exports.registerUser = (reqBody) => {

	const { email, password } = reqBody;
	let newUser = new User({
			email: email,
			password: bcrypt.hashSync(password, 10)
		})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})

}

//User Login Controller
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}

	})

}

//User to Admin Controller
module.exports.setAsAdmin = async (data) => {

	if(data.isAdmin) {
		return User.findById(data.userId).then((user, error) => {
			if(error) {
				return false
			} else {
				user.isAdmin = true;
				return user.save().then(result => result)
			}
		})
	} else {
		return false
	}

}

//User Create Order Controller
module.exports.createOrder = async data => {

	if(!data.isAdmin) {

		const { userId, productId, quantity } = data;
		const multiplier = (quantity === undefined) ? 1 : quantity;
		let userUpdateStatus = await User.findById(userId).then(result => {
			return Product.findById(productId).then(product => {

				if(product.isActive) {
					result.orders.push({productId: productId, quantity: quantity, totalAmount: product.price * multiplier})
					return result.save().then((updatedUserDetails, error) => {
						if(error) {
							return false
						} else {
							return updatedUserDetails
						}
					})
				} else {
					return false
				}
				
			})
		});

		let productUpdateStatus = await Product.findById(productId).then(product => {
			return User.findById(userId).then(result => {

				if(product.isActive) {
					product.buyers.push({userId: userId, quantity: quantity, totalAmount: product.price * multiplier})
					return product.save().then((updatedProductDetails, error) => {
						if(error) {
							return false
						} else {
							return updatedProductDetails
						}
					})
				} else {
					return false
				}
				
			})
		});
		
		if(userUpdateStatus && productUpdateStatus) {
			return "Order Successful"
		} else {
			return "Order Unsuccessful"
		}

	} else {
		return false
	}

}

module.exports.getAllOrders = async isAdmin => {

	if(isAdmin) {

		return Product.find({}).then(result => result);

	} else {
		return false
	}

}

module.exports.getUserOrders = async data => {

	const { userId, isAdmin } = data
	if(!isAdmin) {

		return User.findById(userId).then(result => result.orders);

	} else {
		return false
	}

}

