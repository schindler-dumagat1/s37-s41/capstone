const User = require("../models/User")
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Create Product Controller
module.exports.createProduct = async data => {

	if(data.isAdmin) {

		const { name, description, price, isActive } = data.productDetails;
		let newProduct = new Product({
			name: name,
			description: description,
			price: price,
			isActive: isActive
		})

		return newProduct.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return product
			}
		})

	} else {
		return false
	}

}

//Retrieve All Active Products Controller
module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then((result, error) => {
		if(error) {
			return false
		} else {
			return result
		}
	})

}

//Get Single Product Controller
module.exports.getSingleProduct = productId => {

	return Product.findById(productId).then((result, error) => {
		if(error) {
			return false
		} else {
			return result
		}
	})
}

//Update Product Controller
module.exports.updateProduct = async data => {

	const { isAdmin, productId, name, description, price, isActive } = data;
	if(isAdmin) {
		return Product.findById(productId).then((result, error) => {
			if(error) {
				return false
			} else {
				if(name) {
					result.name = name;
				}
				if(description) {
					result.description = description;
				}
				if(price) {
					result.price = price;
				}
				if(isActive !== undefined) {
					result.isActive = isActive;
				}

				return result.save().then((updatedProduct, error) => {
					if(error) {
						return false
					} else {
						return updatedProduct
					}
				})

			}
		})
	} else {
		return false
	}

}

//Archive Product Controller
module.exports.archiveProduct = async data => {

	const { isAdmin, productId } = data
	if(isAdmin) {
		return Product.findById(productId).then((result, error) => {
			if(error) {
				return false
			} else {
				result.isActive = false

				return result.save().then((archivedProduct, error) => {
					if(error) {
						return false
					} else {
						return archivedProduct
					}
				})
			}
		})
	} else {
		return false
	}
}
