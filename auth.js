const jwt = require("jsonwebtoken");

const secret = "E-CommerceAPI";

//Token Creation
module.exports.createAccessToken = (user) => {

	//When the user login, the token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//Generate a JSON web token using the jwt's sign method
	//Generates the token using the form data and the secrete code with no additional option provided.
	return jwt.sign(data, secret, {});

}

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send(false)
			} else {
				next();
			}

		})
	} else {
		return res.send(false)
	}
}

module.exports.decode = (token) => {

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null
	}

}
