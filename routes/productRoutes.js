const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers")
const auth = require("../auth")

//Create Product Route
router.post("/", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productDetails: req.body
	}
	 productController.createProduct(data).then(resultFromController => res.send(resultFromController));

})

//Retrieve All Active Products Route
router.get("/", (req, res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))

})

//Get Single Product Route
router.get("/:productId", (req, res) => {

	productController.getSingleProduct(req.params.productId).then(resultFromController => res.send(resultFromController));

})

//Update Product Route
router.put("/:productId", auth.verify, (req, res) => {

	const { name, description, price, isActive } = req.body;
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId,
		name: name,
		description: description,
		price: price,
		isActive: isActive
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));

})

//Archive Product Route
router.put("/:productId/archive", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId,
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;
