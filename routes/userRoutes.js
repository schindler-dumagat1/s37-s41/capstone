const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers")
const auth = require("../auth")

//User Registration Route
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

//User Login Route
router.get("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)); 
})

//User to Admin Route
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.params.userId
	}
	 userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

//User Create Order Route
router.post("/checkout", auth.verify, (req, res) => {

	const {id, isAdmin} = auth.decode(req.headers.authorization);
	const { productId, quantity } = req.body;
	let data = {
		userId: id,
		isAdmin: isAdmin,
		productId: productId,
		quantity: quantity
	}
	userController.createOrder(data).then(resultFromController => res.send(resultFromController));

})

//Retrieve All Orders Route
router.get("/orders", auth.verify, (req, res) => {

	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));

})

//Retrieve User Order Route
router.get("/myOrders", auth.verify, (req, res) => {

	const {id, isAdmin} = auth.decode(req.headers.authorization);
	let data = {
		userId: id,
		isAdmin: isAdmin
	}
	 	userController.getUserOrders(data).then(resultFromController => res.send(resultFromController));

})

module.exports = router;
